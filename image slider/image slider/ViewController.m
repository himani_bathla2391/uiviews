//
//  ViewController.m
//  image slider
//
//  Created by Clicklabs14 on 9/15/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UIView *view1;
UIView *view2;
UIView *view3;
UIView *view4;

UILabel *label;
UIButton *button;
UITextField *textfield;
UIImageView *img;

- (void)viewDidLoad {
    [super viewDidLoad];
    view1= [[UIView alloc]init ];
    view1.frame = CGRectMake(30, 100, 150, 150);
    view1.backgroundColor= [UIColor yellowColor];
    [self.view addSubview: view1];
    
    view2= [[UIView alloc]initWithFrame:CGRectMake(200, 100, 150, 150)];
    view2.backgroundColor= [UIColor redColor];
    [self.view addSubview: view2];
    
    view3= [[UIView alloc]initWithFrame:CGRectMake(30, 400, 150, 150)];
    view3.backgroundColor= [UIColor blueColor];
    [self.view addSubview: view3];
    
    view4= [[UIView alloc]initWithFrame:CGRectMake(200, 400, 150, 150)];
    view4.backgroundColor= [UIColor greenColor];
    [self.view addSubview: view4];
    
    label= [[UILabel alloc]init ];
    label.frame= CGRectMake(40, 55, 70, 40);
    label.backgroundColor= [UIColor orangeColor];
    label.textColor= [UIColor whiteColor];
    label.text= @"    label";
    [view1 addSubview: label];
    
    textfield= [[UITextField alloc]init ];
    textfield.frame= CGRectMake(40, 55, 70, 40);
    textfield.backgroundColor= [UIColor whiteColor];
    textfield.textColor= [UIColor blackColor];
    textfield.borderStyle= UITextBorderStyleRoundedRect;
    textfield.placeholder= @"    textfield";
    [textfield setFont:[UIFont boldSystemFontOfSize:8]];
    [view2 addSubview: textfield];
    
    button= [[UIButton alloc]init ];
    button.frame= CGRectMake(40, 55, 70, 40);
    button.backgroundColor= [UIColor blackColor];
    [button setTitle: @"  button" forState:(UIControlStateNormal)];
    [button addTarget: self action: @selector(button:) forControlEvents:(UIControlEventTouchUpInside)];
    [view3 addSubview: button];

    img= [[UIImageView alloc]init ];
    img.frame= CGRectMake(40, 55, 70, 50);
    img.backgroundColor= [UIColor brownColor];
    img.image= [UIImage imageNamed:@"be-bold-for-Christ.jpg"];
    [view4 addSubview: img];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
