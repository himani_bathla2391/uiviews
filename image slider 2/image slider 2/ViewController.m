//
//  ViewController.m
//  image slider 2
//
//  Created by Clicklabs14 on 9/16/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UIView *view1;
UIView *view2;
UIView *view3;
UIView *view4;
UIView *view5;
UIView *view6;
UIView *view7;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    view1= [[UIView alloc]init ];
    view1.frame = CGRectMake(40, 40, 300, 300);
    view1.backgroundColor= [UIColor yellowColor];
    view1.layer.cornerRadius= view1.frame.size.height/2;
    [self.view addSubview: view1];
    
    view2= [[UIView alloc]initWithFrame:CGRectMake(25, 25, 250, 250)];
    view2.backgroundColor= [UIColor redColor];
    view2.layer.cornerRadius=125;
    [view1 addSubview: view2];
    
    view3= [[UIView alloc]initWithFrame:CGRectMake(25, 25, 200, 200)];
    view3.backgroundColor= [UIColor blueColor];
    view3.layer.cornerRadius=100;
    [view2 addSubview: view3];
    
    view4= [[UIView alloc]initWithFrame:CGRectMake(25, 25, 150, 150)];
    view4.backgroundColor= [UIColor greenColor];
    view4.layer.cornerRadius= 75;
    [view3 addSubview: view4];
    

    view5= [[UIView alloc]initWithFrame:CGRectMake(25, 25, 100, 100)];
    view5.backgroundColor= [UIColor purpleColor];
    view5.layer.cornerRadius= 50;
    [view4 addSubview: view5];
    
    view6= [[UIView alloc]initWithFrame:CGRectMake(25, 25, 50, 50)];
    view6.backgroundColor= [UIColor brownColor];
    view6.layer.cornerRadius= 25;
    [view5 addSubview: view6];
    
    view7= [[UIView alloc]initWithFrame:CGRectMake(20, 20, 10, 10)];
    view7.backgroundColor= [UIColor blackColor];
    view7.layer.cornerRadius= 5;
    [view6 addSubview: view7];

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
