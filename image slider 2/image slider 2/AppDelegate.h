//
//  AppDelegate.h
//  image slider 2
//
//  Created by Clicklabs14 on 9/16/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

